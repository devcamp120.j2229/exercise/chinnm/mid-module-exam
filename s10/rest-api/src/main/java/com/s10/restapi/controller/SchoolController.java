package com.s10.restapi.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.s10.restapi.model.School;
import com.s10.restapi.service.SchoolService;

@RestController
public class SchoolController {
    @Autowired
    static SchoolService schools;

    @CrossOrigin
    // api trả ra danh sách các trường
    @GetMapping("/schools")
    public ArrayList<School> getSchoolList() throws Exception {
        ArrayList<School> allSchools = SchoolService.getListSchool();
        return allSchools;
    }

    // api trả ra dữ liệu của 1 truongqf học theo id
    @GetMapping("/school")
    public Map<String, Object> getSchoolById(@RequestParam(required = true, name = "schoolId") int id)
            throws Exception {
        Map<String, Object> Object = new HashMap<String, Object>();
        School school = null;
        int i = 0;
        boolean isSchoolFound = false;

        while (!isSchoolFound == true && i < SchoolService.getListSchool().size()) {
            if (SchoolService.getListSchool().get(i).getId() == id) {
                school = SchoolService.getListSchool().get(i);
                isSchoolFound = true;
                Object.put("school", school);
                Object.put("status", new String("ok"));
            } else {
                i++;
                Object.put("school", null);
                Object.put("status", new String("not found"));
            }
        }
        return Object;
    }

    // api trả ra danh sách trường có số học sinh lớn hơn tham số
    @GetMapping("/school-by-number-students")
    public Map<String, Object> getSchoolByNumberStudents(@RequestParam(required = true, name = "noNumber") int number)
            throws Exception {
        Map<String, Object> Object = new HashMap<String, Object>();
        ArrayList<School> listSchool = new ArrayList<>();

        for (int i = 0; i < SchoolService.getListSchool().size(); i++) {
            if (SchoolService.getListSchool().get(i).getTotalStudent() > number) {
                listSchool.add(SchoolService.getListSchool().get(i));
                Object.put("school", listSchool);
                Object.put("status", new String("ok"));
            } else {
                Object.put("school", null);
                Object.put("status", new String("not found"));
            }
        }
        return Object;
    }

}
