package com.s10.restapi.service;

import java.util.ArrayList;

import com.s10.restapi.model.ClassRoom;

public class ClassroomService {
    private static ArrayList<ClassRoom> listClassroomOfNguyenHueSchool = new ArrayList<>();
    private static ArrayList<ClassRoom> listClassroomOfNguyenTraiSchool = new ArrayList<>();
    private static ArrayList<ClassRoom> listClassroomOfNguyenKhuyenSchool = new ArrayList<>();
    private static ArrayList<Object> listAllSchool = new ArrayList<>();
    static {
        ClassRoom classRoom1 = new ClassRoom(101, "10A1", 33);
        ClassRoom classRoom2 = new ClassRoom(102, "11A2", 44);
        ClassRoom classRoom3 = new ClassRoom(103, "12A3", 35);
        listClassroomOfNguyenHueSchool.add(classRoom1);
        listClassroomOfNguyenHueSchool.add(classRoom2);
        listClassroomOfNguyenHueSchool.add(classRoom3);
        ClassRoom classRoom4 = new ClassRoom(104, "10B1", 44);
        ClassRoom classRoom5 = new ClassRoom(105, "11B2", 52);
        ClassRoom classRoom6 = new ClassRoom(106, "12B3", 45);
        listClassroomOfNguyenTraiSchool.add(classRoom4);
        listClassroomOfNguyenTraiSchool.add(classRoom5);
        listClassroomOfNguyenTraiSchool.add(classRoom6);
        ClassRoom classRoom7 = new ClassRoom(107, "10C1", 55);
        ClassRoom classRoom8 = new ClassRoom(108, "11C2", 54);
        ClassRoom classRoom9 = new ClassRoom(109, "12C3", 53);
        listClassroomOfNguyenKhuyenSchool.add(classRoom7);
        listClassroomOfNguyenKhuyenSchool.add(classRoom8);
        listClassroomOfNguyenKhuyenSchool.add(classRoom9);

        listAllSchool.add(listClassroomOfNguyenHueSchool);
        listAllSchool.add(listClassroomOfNguyenKhuyenSchool);
        listAllSchool.add(listClassroomOfNguyenTraiSchool);

    }

    public static ArrayList<ClassRoom> getListClassroomOfNguyenHueSchool() {
        return listClassroomOfNguyenHueSchool;
    }

    public static void setListClassroomOfNguyenHueSchool(ArrayList<ClassRoom> listClassroomOfNguyenHueSchool) {
        ClassroomService.listClassroomOfNguyenHueSchool = listClassroomOfNguyenHueSchool;
    }

    public static ArrayList<ClassRoom> getListClassroomOfNguyenTraiSchool() {
        return listClassroomOfNguyenTraiSchool;
    }

    public static void setListClassroomOfNguyenTraiSchool(ArrayList<ClassRoom> listClassroomOfNguyenTraiSchool) {
        ClassroomService.listClassroomOfNguyenTraiSchool = listClassroomOfNguyenTraiSchool;
    }

    public static ArrayList<ClassRoom> getListClassroomOfNguyenKhuyenSchool() {
        return listClassroomOfNguyenKhuyenSchool;
    }

    public static void setListClassroomOfNguyenKhuyenSchool(ArrayList<ClassRoom> listClassroomOfNguyenKhuyenSchool) {
        ClassroomService.listClassroomOfNguyenKhuyenSchool = listClassroomOfNguyenKhuyenSchool;
    }

    public static ArrayList<Object> getListAllSchool() {
        return listAllSchool;
    }

    public static void setListAllSchool(ArrayList<Object> listAllSchool) {
        ClassroomService.listAllSchool = listAllSchool;
    }

}
