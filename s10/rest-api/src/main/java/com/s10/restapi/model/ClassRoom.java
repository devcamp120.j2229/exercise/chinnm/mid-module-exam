package com.s10.restapi.model;

public class ClassRoom {
    private int id;
    private String name;
    private int noStudent;

    public ClassRoom(int id, String name, int noStudent) {
        this.id = id;
        this.name = name;
        this.noStudent = noStudent;
    }

    public ClassRoom() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getNoStudent() {
        return noStudent;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNoStudent(int noStudent) {
        this.noStudent = noStudent;
    }

}
