package com.s10.restapi.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.s10.restapi.model.ClassRoom;
import com.s10.restapi.service.ClassroomService;
import com.s10.restapi.service.SchoolService;

@RestController
public class ClassroomController {
    @Autowired
    static ClassroomService classrooms;
    static SchoolService schoolService;

    @CrossOrigin
    // api trả ra danh sách các lớp
    @GetMapping("/classrooms")
    public ArrayList<Object> getClassroomList() throws Exception {
        ArrayList<Object> allClassRooms = new ArrayList<>();
        for (int i = 0; i < SchoolService.getListSchool().size(); i++) {
            allClassRooms.add(SchoolService.getListSchool().get(i).getClassRooms());
        }
        return allClassRooms;
    }

    // api trả ra danh sách các lớp có số học sinh lớn hơn tham số
    @GetMapping("/classroom-by-number-students")
    public Map<String, Object> getSchoolByNumberStudents(@RequestParam(required = true, name = "noNumber") int number)
            throws Exception {
        Map<String, Object> Object = new HashMap<String, Object>();
        ArrayList<ClassRoom> listClassRoom = new ArrayList<>();

        for (int i = 0; i < SchoolService.getListSchool().size(); i++) {
            for (int classroomIndex = 0; classroomIndex < SchoolService.getListSchool().get(i).getClassRooms()
                    .size(); classroomIndex++) {
                if (SchoolService.getListSchool().get(i).getClassRooms().get(classroomIndex).getNoStudent() > number) {

                    listClassRoom.add(SchoolService.getListSchool().get(i).getClassRooms().get(classroomIndex));

                }
            }
        }
        if (listClassRoom.size() > 0) {
            Object.put("classroom", listClassRoom);
            Object.put("status", new String("ok"));

        } else {
            Object.put("classroom", null);
            Object.put("status", new String("not found"));

        }
        return Object;
    }
}
