package com.s10.restapi.model;

import java.util.ArrayList;

public class School {
    public School(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    private int id;
    private String name;
    private String address;
    private ArrayList<ClassRoom> classRooms;

    public School(int id, String name, String address, ArrayList<ClassRoom> classRooms) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.classRooms = classRooms;
    }

    public School() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public ArrayList<ClassRoom> getClassRooms() {
        return classRooms;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setClassRooms(ArrayList<ClassRoom> classRooms) {
        this.classRooms = classRooms;
    }

    public int getTotalStudent() {
        int total = 0;
        for (int i = 0; i < classRooms.size(); i++) {
            total += classRooms.get(i).getNoStudent();
        }
        return total;
    }

}
