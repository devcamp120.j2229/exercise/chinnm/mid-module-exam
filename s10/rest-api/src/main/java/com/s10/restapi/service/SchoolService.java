package com.s10.restapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import com.s10.restapi.model.School;

public class SchoolService {
    private static ArrayList<School> listSchool = new ArrayList<>();
    @Autowired

    static ClassroomService classroom;
    static {
        School nguyenHueSchool = new School(1, "Nguyễn Huệ", "56 Nguyễn Huệ,Vĩnh Long", null);
        School nguyenTraiSchool = new School(2, "Nguyễn Trãi", "75 Nguyễn Trãi,Vĩnh Long", null);
        School nguyenKhuyenSchool = new School(3, "Nguyễn Khuyến", "24 Nguyễn Khuyến,Vĩnh Long", null);
        listSchool.add(nguyenHueSchool);
        listSchool.add(nguyenTraiSchool);
        listSchool.add(nguyenKhuyenSchool);
        for (int i = 0; i < listSchool.size(); i++) {
            if (listSchool.get(i).getName() == "Nguyễn Huệ") {
                listSchool.get(i).setClassRooms(ClassroomService.getListClassroomOfNguyenHueSchool());
            } else if (listSchool.get(i).getName() == "Nguyễn Trãi") {
                listSchool.get(i).setClassRooms(ClassroomService.getListClassroomOfNguyenTraiSchool());
            } else if (listSchool.get(i).getName() == "Nguyễn Khuyến") {
                listSchool.get(i).setClassRooms(ClassroomService.getListClassroomOfNguyenKhuyenSchool());
            }
        }
    }

    public static ArrayList<School> getListSchool() {
        return listSchool;
    }

    public static void setListSchool(ArrayList<School> listSchool) {
        SchoolService.listSchool = listSchool;
    }

    public static ClassroomService getClassroom() {
        return classroom;
    }

    public static void setClassroom(ClassroomService classroom) {
        SchoolService.classroom = classroom;
    }

}
