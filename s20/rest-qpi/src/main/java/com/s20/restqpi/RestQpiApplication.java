package com.s20.restqpi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestQpiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestQpiApplication.class, args);
	}

}
