package com.s20.restqpi.controller;

import java.util.HashSet;
import java.util.Set;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StringController {
    @CrossOrigin
    @GetMapping("/string-reverse")
    public String reverseString(@RequestParam(required = true, name = "string") String stringInput) {
        String stringReversed = "";
        if (stringInput == "") {
            return "Missing required parameter";
        } else {
            for (int i = 0; i < stringInput.length(); i++) {
                stringReversed = stringInput.charAt(i) + stringReversed;
            }
        }
        return stringReversed;

    }

    @GetMapping("/string-palindrome")
    public String isPalindrome(@RequestParam(required = true, name = "string") String stringInput) {
        String stringReversed = "";
        if (stringInput == "") {
            return "Missing required parameter";
        } else {
            for (int i = 0; i < stringInput.length(); i++) {
                stringReversed = stringInput.charAt(i) + stringReversed;
            }
        }

        if (stringInput.equals(stringReversed)) {
            return stringInput + " is palindrome";
        } else {
            return stringInput + " is not palindrome";
        }

    }

    @GetMapping("/string-remove-duplicate")
    public String removeDuplicateCharacter(@RequestParam(required = true, name = "string") String stringInput) {
        Set<Character> presentCharacters = new HashSet<>();
        StringBuilder stringBuilder = new StringBuilder();
        if (stringInput == "") {
            return "Missing required parameter";
        } else {
            for (int i = 0; i < stringInput.length(); i++) {
                if (!presentCharacters.contains(stringInput.charAt(i))) {
                    stringBuilder.append(stringInput.charAt(i));
                    presentCharacters.add(stringInput.charAt(i));
                }
            }

        }

        return stringBuilder.toString();

    }

    @GetMapping("/string-conected")
    public String conectedString(@RequestParam(required = true, name = "string1") String stringInput1,
            @RequestParam(required = true, name = "string2") String stringInput2) {

        if (stringInput1 == "" || stringInput2 == "") {
            return "Missing required parameter";
        } else {
            String result = "";
            if (stringInput1.length() == stringInput2.length()) {
                result = stringInput1 + stringInput2;
            } else if (stringInput1.length() > stringInput2.length()) {
                StringBuilder stringBuilder = new StringBuilder();
                String string1Reverse = "";
                for (int i = 0; i < stringInput1.length(); i++) {
                    string1Reverse = stringInput1.charAt(i) + string1Reverse;
                }

                for (int i = 0; i < string1Reverse.length(); i++) {
                    if (i < stringInput2.length()) {
                        stringBuilder.append(string1Reverse.charAt(i));
                    }
                }
                String string1Builder = stringBuilder.toString();
                String string1BuilderReversed = "";
                for (int i = 0; i < string1Builder.length(); i++) {
                    string1BuilderReversed = string1Builder.charAt(i) + string1BuilderReversed;
                }
                result = string1BuilderReversed + stringInput2;

            } else if (stringInput1.length() < stringInput2.length()) {
                StringBuilder stringBuilder = new StringBuilder();
                String string2Reverse = "";
                for (int i = 0; i < stringInput2.length(); i++) {
                    string2Reverse = stringInput2.charAt(i) + string2Reverse;
                }

                for (int i = 0; i < string2Reverse.length(); i++) {
                    if (i < stringInput1.length()) {
                        stringBuilder.append(string2Reverse.charAt(i));
                    }
                }
                String string2Builder = stringBuilder.toString();
                String string2BuilderReversed = "";
                for (int i = 0; i < string2Builder.length(); i++) {
                    string2BuilderReversed = string2Builder.charAt(i) + string2BuilderReversed;
                }
                result = stringInput1 + string2BuilderReversed;
            }
            return result;
        }

    }
}
